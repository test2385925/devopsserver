import telegram

def send_telegram_notification(bot_token, chat_id, message):
    bot = telegram.Bot(token=bot_token)
    bot.send_message(chat_id=chat_id, text=message)

# Replace with your bot's API key and chat ID
bot_token = "7417219861:AAGht8rkh9_KEnzJfwvF9dHfSGZDWZwd2WE" 
chat_id = "-4244201586" 

trigger_name = sys.argv[1]
hostname = sys.argv[2]
trigger_value = sys.argv[3]
severity = sys.argv[4]

# Construct the message 
message = f"Zabbix alert on {hostname}: {trigger_name} triggered.  Value: {trigger_value}. Severity: {severity}"

# Send the notification
send_telegram_notification(bot_token, chat_id, message)